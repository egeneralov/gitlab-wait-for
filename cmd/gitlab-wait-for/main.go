package main

import (
	"context"
	"errors"
	"flag"
	"os"

	"github.com/chapsuk/grace"
	"go.uber.org/zap"

	"gitlab.com/egeneralov/gitlab-wait-for"
)

var (
	cfg gitlab_wait_for.Config
	mgr *gitlab_wait_for.Manager
	log *zap.Logger
)

func init() {
	var err error
	if cl, err := zap.NewProduction(); err != nil {
		panic(err)
	} else {
		log = cl.With(zap.String("app", "gitlab-wait-for"))
	}
	tmp := gitlab_wait_for.Config{}
	log.Debug("initializing configuration", zap.Strings("files", tmp.Files()))
	cfg, err = gitlab_wait_for.NewConfig()
	if err != nil {
		if errors.Is(err, flag.ErrHelp) {
			os.Exit(0)
		}
		log.Fatal("failed to load config", zap.Error(err))
	}
	log.Info(
		"loaded configuration",
		zap.String("url", cfg.URL),
		zap.String("ref", cfg.Ref),
		zap.Strings("jobs", cfg.Jobs),
		zap.Int("project_id", cfg.ProjectID),
		zap.String("job_timeout", cfg.Timeouts.Pipeline.String()),
	)
}

func main() {
	var (
		err         error
		shutdownCtx = grace.ShutdownContext(context.Background())
	)
	if mgr, err = gitlab_wait_for.NewManager(cfg, log.With(zap.String("subsystem", "manager"))); err != nil {
		log.Fatal("failed to create manager", zap.Error(err))
	}
	if err := mgr.AIO(shutdownCtx); err != nil {
		log.Fatal("failed", zap.Error(err), zap.Int("project_id", cfg.ProjectID))
	}
	log.Info("finished")
}
