package gitlab_wait_for

import (
	"time"

	"github.com/cristalhq/aconfig"
	"github.com/cristalhq/aconfig/aconfigdotenv"
	"github.com/cristalhq/aconfig/aconfighcl"
	"github.com/cristalhq/aconfig/aconfigtoml"
	"github.com/cristalhq/aconfig/aconfigyaml"
)

type Config struct {
	URL       string   `default:"https://gitlab.com/api/v4" usage:"GitLab instance API endpoint" json:"url" yaml:"url"`
	Ref       string   `default:"master" usage:"The branch or tag to run the pipeline on." json:"ref" yaml:"ref"`
	Token     string   `usage:"Personal access token with scopes [api, read_api]" json:"token" yaml:"token"`
	Jobs      []string `usage:"Comma-separated list of manual jobs" json:"jobs" yaml:"jobs"`
	ProjectID int      `usage:"Project id to manipulate" json:"project_id" yaml:"project_id"`
	Timeouts  struct {
		Pipeline time.Duration `usage:"max time for pipeline to complete" default:"1h" json:"pipeline" yaml:"pipeline"`
		Job      time.Duration `usage:"max time for job to complete" default:"1h" json:"job" yaml:"job"`
		Retry    time.Duration `usage:"max time for job to become retry state" default:"2m" json:"retry" yaml:"retry"`
	} `json:"timeouts" yaml:"timeouts"`
	Limits struct {
		JobsPerPipeline int `usage:"count of requested pipelines" default:"100" json:"jobs_per_pipeline" yaml:"jobs_per_pipeline"`
	}
}

func (c *Config) Files() (result []string) {
	for _, prefix := range []string{"", "."} {
		for _, name := range []string{"gitlab-wait-for", "gwf"} {
			for _, ending := range []string{"hcl", "yaml", "yml", "toml", "json", "env"} {
				result = append(result, prefix+name+"."+ending)
			}
		}
	}
	return
}

func (c *Config) Load() error {
	acfg := aconfig.Config{
		EnvPrefix:        "GWF",
		FlagDelimiter:    "",
		AllFieldRequired: true,
		AllowDuplicates:  false,
		DontGenerateTags: false,
		MergeFiles:       true,
		FileFlag:         "config",
		FileDecoders: map[string]aconfig.FileDecoder{
			".hcl":  aconfighcl.New(),
			".yml":  aconfigyaml.New(),
			".yaml": aconfigyaml.New(),
			".toml": aconfigtoml.New(),
			".env":  aconfigdotenv.New(),
		},
	}
	acfg.Files = c.Files()
	return aconfig.LoaderFor(c, acfg).Load()
}

func NewConfig() (Config, error) {
	c := Config{}
	if err := c.Load(); err != nil {
		return Config{}, err
	}
	return c, nil
}
