package gitlab_wait_for

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
	"go.uber.org/zap"
	"golang.org/x/exp/slices"
)

type Manager struct {
	config Config
	client *gitlab.Client
	log    *zap.Logger
}

func (m *Manager) init(cfg Config) error {
	m.config = cfg
	var err error
	m.client, err = gitlab.NewClient(
		m.config.Token,
		gitlab.WithBaseURL(m.config.URL),
		gitlab.WithHTTPClient(&http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   180 * time.Second,
					KeepAlive: 30 * time.Second,
				}).DialContext,
				MaxIdleConns:          10,
				IdleConnTimeout:       180 * time.Second,
				TLSHandshakeTimeout:   180 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
				ForceAttemptHTTP2:     true,
			},
			Timeout: time.Second * 180,
		}),
		//gitlab.WithoutRetries(),
		//gitlab.WithCustomRetryMax(1),
	)
	if err != nil {
		return err
	}
	return nil
}

func (m *Manager) CreatePipeline() (*gitlab.Pipeline, error) {
	opt := &gitlab.CreatePipelineOptions{Ref: gitlab.String(m.config.Ref)}
	p, _, err := m.client.Pipelines.CreatePipeline(m.config.ProjectID, opt)
	return p, err
}

func (m *Manager) Wait4Job(ctx context.Context, job *gitlab.Job) (*gitlab.Job, error) {
	ctx, cancel := context.WithTimeout(ctx, m.config.Timeouts.Job)
	defer cancel()
	for {
		select {
		case <-ctx.Done():
			return nil, fmt.Errorf("context error: %w", ctx.Err())
		default:
			j, _, err := m.client.Jobs.GetJob(m.config.ProjectID, job.ID)
			if err != nil {
				return nil, fmt.Errorf("failed to get job (status): %w", err)
			}
			if !slices.Contains([]string{
				"created",
				"pending",
				"running",
				"failed",
				"success",
				"canceled",
				"skipped",
				"waiting_for_resource",
				"manual",
			}, j.Status) {
				return nil, fmt.Errorf("job status not valid: %s", j.Status)
			}
			if j.Status == "failed" {
				if s, err := json.Marshal(j); err != nil {
					panic(err)
				} else {
					return nil, fmt.Errorf("job has failed status: %s", string(s))
				}
			}
			if slices.Contains([]string{
				"created",
				"pending",
				"running",
				"canceled",
				"skipped",
				"waiting_for_resource",
				"manual",
			}, j.Status) {
				continue
			}
			if j.Status == "success" {
				return j, nil
			}
			return nil, nil
		}
	}
}

func (m *Manager) Wait4Pipeline(ctx context.Context, pipeline *gitlab.Pipeline) (*gitlab.Pipeline, error) {
	ctx, cancel := context.WithTimeout(ctx, m.config.Timeouts.Pipeline)
	defer cancel()
	for {
		select {
		case <-ctx.Done():
			return nil, fmt.Errorf("context error: %w", ctx.Err())
		default:
			p, _, err := m.client.Pipelines.GetPipeline(m.config.ProjectID, pipeline.ID)
			if err != nil {
				return nil, fmt.Errorf("failed to get pipeline (status): %w", err)
			}
			if !slices.Contains([]string{
				"created",
				"waiting_for_resource",
				"preparing",
				"pending",
				"running",
				"success",
				//"failed",
				"canceled",
				"skipped",
				"manual",
				"scheduled",
			}, p.Status) {
				return nil, fmt.Errorf("pipeline status not valid: %s", p.Status)
			}
			if p.Status == "failed" {
				if s, err := json.Marshal(p); err != nil {
					panic(err)
				} else {
					return nil, fmt.Errorf("pipeline has failed status: %s", string(s))
				}
			}
			if slices.Contains([]string{
				"created",
				"waiting_for_resource",
				"preparing",
				"pending",
				"running",
			}, p.Status) {
				continue
			}
			if slices.Contains([]string{
				"success",
				"skipped",
				"manual",
			}, p.Status) {
				return p, nil
			}
		}
	}
}

func (m *Manager) RunJob(p *gitlab.Pipeline, name string) (*gitlab.Job, error) {
	jobs, _, err := m.client.Jobs.ListPipelineJobs(m.config.ProjectID, p.ID, &gitlab.ListJobsOptions{
		ListOptions: gitlab.ListOptions{
			Page:    0,
			PerPage: m.config.Limits.JobsPerPipeline,
		},
		Scope:          nil,
		IncludeRetried: gitlab.Bool(true),
	})
	if err != nil {
		return nil, fmt.Errorf("failed to list jobs in pipeline %s: %w", p.WebURL, err)
	}
	var jobNames []string
	for _, job := range jobs {
		jobNames = append(jobNames, job.Name)
	}
	m.log.Debug("available", zap.Strings("jobs", jobNames))
	if !slices.Contains(jobNames, name) {
		return nil, fmt.Errorf("job '%s' not founded in pipeline %d", name, p.ID)
	}
	for _, job := range jobs {
		if job.Name == name {
			m.log.Debug("running job", zap.String("name", name))
			j, _, err := m.client.Jobs.PlayJob(m.config.ProjectID, job.ID, &gitlab.PlayJobOptions{})
			if err != nil {
				return nil, fmt.Errorf("failed to play job '%s' in pipeline %d: %w", name, p.ID, err)
			}
			m.log.Info("started job", zap.String("name", name), zap.String("url", j.WebURL))
			return j, nil
		}
	}
	return nil, fmt.Errorf("pipeline has no jobs")
}

func (m *Manager) RetryJob(ctx context.Context, job *gitlab.Job) (*gitlab.Job, error) {
	ctx, cancel := context.WithTimeout(ctx, m.config.Timeouts.Retry)
	defer cancel()
	j, _, err := m.client.Jobs.RetryJob(m.config.ProjectID, job.ID, gitlab.WithContext(ctx))
	if err != nil {
		return nil, fmt.Errorf("failed to retry job %d in pipeline %d in project %d: %w", job.ID, job.Pipeline.ID, m.config.ProjectID, err)
	}
	return j, nil
}

func (m *Manager) AIO(ctx context.Context) error {
	p, err := m.CreatePipeline()
	if err != nil {
		return fmt.Errorf("failed to create pipeline: %w", err)
	}
	m.log.Info("waiting for pipeline", zap.String("url", p.WebURL))
	if _, err := m.Wait4Pipeline(ctx, p); err != nil {
		return fmt.Errorf("failed to wait for pipeline valid status: %w", err)
	}
	m.log.Info("running jobs", zap.Strings("queue", m.config.Jobs))
	for _, jobName := range m.config.Jobs {
		if jobName == "" {
			m.log.Warn("skipping job", zap.String("name", jobName), zap.Int("pipeline", p.ID))
			continue
		}
		m.log.Debug("running job", zap.String("name", jobName), zap.String("pipeline", p.WebURL))
		job, err := m.RunJob(p, jobName)
		if err != nil {
			return fmt.Errorf("failed to run job '%s' in pipeline %d: %w", jobName, p.ID, err)
		}
		if _, err := m.Wait4Job(ctx, job); err != nil {
			return fmt.Errorf("failed to wait for job %d for pipeline %d: %w", job.ID, p.ID, err)
		}
	}
	return nil
}

func NewManager(cfg Config, log *zap.Logger) (*Manager, error) {
	if log == nil {
		panic("nil logger")
	}
	if cfg.Ref == "" {
		return nil, fmt.Errorf("empty ref")
	}
	if cfg.ProjectID < 1 {
		return nil, fmt.Errorf("project must be > 0 - first used for gitlab service functions")
	}
	if !strings.HasSuffix(cfg.URL, "/api/v4") {
		return nil, fmt.Errorf("must ends with /api/v4")
	}
	if cfg.Timeouts.Pipeline < 10*time.Second {
		return nil, fmt.Errorf("minimal timeout 10s")
	}
	m := &Manager{
		config: cfg,
		log:    log,
	}
	if err := m.init(cfg); err != nil {
		return nil, fmt.Errorf("failed to initialise gitlab client: %w", err)
	}
	return m, nil
}
