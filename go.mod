module gitlab.com/egeneralov/gitlab-wait-for

go 1.19

require (
	github.com/chapsuk/grace v0.5.0
	github.com/cristalhq/aconfig v0.18.3
	github.com/cristalhq/aconfig/aconfigdotenv v0.17.1
	github.com/cristalhq/aconfig/aconfighcl v0.17.1
	github.com/cristalhq/aconfig/aconfigtoml v0.17.1
	github.com/cristalhq/aconfig/aconfigyaml v0.17.1
	github.com/xanzy/go-gitlab v0.80.2
	go.uber.org/zap v1.24.0
	golang.org/x/exp v0.0.0-20230304125523-9ff063c70017
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.1 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/net v0.4.0 // indirect
	golang.org/x/oauth2 v0.3.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
